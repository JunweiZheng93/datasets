# Open Source Small Datasets (OSSD)

This is the repository for some useful open source small datasets, for example, mnist, svhn and so on. All datasets in this repository are in `.mat` format.

## Usage

There are some useful functions in `utils.py`, including `read_dataset`, which will read the dataset into two `np.ndarray`, that is,  images in `(N, H, W, C)` and  labels in `(N,)`;  `download_dataset`, which will download the dataset from this repository; and so on. For every dataset, there is a pre-trained model in keras`.h5` format and its training/validation tensorboard logs, which helps you to start your academic research quickly. For more information, please check `utils.py`.

## MNIST

MNIST is a dataset of handwritten digits. Every image has 28x28 pixels in grayscale, which means the shape of the image should be `(28, 28, 1)`. The labels are from `0` to `9`, which represent the digit `0` to `9`. There are 60,000 samples in training set and 10,000 samples in test set. The reference link can be found [here](http://yann.lecun.com/exdb/mnist/).

| Images  |   Labels   | Samples in training set | Samples in test set | Validation accuracy |
| :-----: | :--------: | :---------------------: | :-----------------: | :-----------------: |
| 28x28x1 | 10 classes |         60,000          |       10,000        |       99.52%        |

![](https://raw.githubusercontent.com/JunweiZheng93/blog_images/master/20200927005654.png)

## Fashion MNIST

Fashion MNIST is very similar to MNIST. The only thing different from MNIST is just the images pattern. There are 60,000 samples in training set and 10,000 samples in test set. The reference link can be found [here](https://research.zalando.com/welcome/mission/research-projects/fashion-mnist/).

| Images  |   Labels   | Samples in training set | Samples in test set |
| :-----: | :--------: | :---------------------: | :-----------------: |
| 28x28x1 | 10 classes |         60,000          |       10,000        |

![](https://raw.githubusercontent.com/JunweiZheng93/blog_images/master/20200927005856.png)

## CIFAR10

CIFAR10 is a dataset for 10 different classes in RGB format. Every images has 32x32 pixels, which means it has a shape of `(32, 32, 3)`. There are 50,000 samples in training set and 10,000 samples in test set. The reference link can be found [here](https://www.cs.toronto.edu/~kriz/cifar.html).

| Images  |   Labels   | Samples in training set | Samples in test set |
| :-----: | :--------: | :---------------------: | :-----------------: |
| 32x32x3 | 10 classes |         50,000          |       10,000        |

![](https://raw.githubusercontent.com/JunweiZheng93/blog_images/master/20200927010011.png)

## SVHN

SVHN is the abbreviation of Street View House Number, which comes from house numbers in Google Street View images. The original SVHN has 10 classes, 1 for each digit. Digit `1` has label `1`, `9` has label `9` and `0` has label `10`. But in this repository, the labels are preprocessed in MNIST way, which means digit `1` has label `1`, `9` has label `9` and `0` has label `0`. Be careful, there might be some **distracting** digits to the sides of the digit of interest. The reference link can be found [here](http://ufldl.stanford.edu/housenumbers/).

| Images  |   Labels   | Samples in training set | Samples in test set | Validation accuracy |
| :-----: | :--------: | :---------------------: | :-----------------: | :-----------------: |
| 32x32x3 | 10 classes |         73,257          |       26,032        |       96.28%        |

![](https://raw.githubusercontent.com/JunweiZheng93/blog_images/master/20200927012411.png)

## Synthetic Digits

Synthetic Digits is a dataset from Kaggle. This dataset contains 12,000 synthetically generated images of English digits embedded on random backgrounds. The images are generated with varying fonts, colors, scales and rotations. The backgrounds are randomly selected from a subset of COCO dataset. Images in the original dataset has different shape, but in this repository, they have been resized to `(32, 32, 3)`. The reference link can be found [here](https://www.kaggle.com/prasunroy/synthetic-digits).

| Images  |   Labels   | Samples in training set | Samples in test set |
| :-----: | :--------: | :---------------------: | :-----------------: |
| 32x32x3 | 10 classes |         10,000          |        2,000        |

![](https://raw.githubusercontent.com/JunweiZheng93/blog_images/master/20200927015127.png)

