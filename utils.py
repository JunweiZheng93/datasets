import scipy.io
import numpy as np
import os
import requests
from tqdm import tqdm
import matplotlib.pyplot as plt
import math


def download_dataset(url: str, saved_path: str, file_name: str) -> str:
    """
    Args:
        url:
            str. Url of the dataset to be downloaded. For example, the mnist trainingset in this repository should
            be "https://gitlab.com/JunweiZheng93/datasets/-/raw/master/mnist/mnist_trainingset.mat?inline=false"
        saved_path:
            str. Path to save the dataset. For example, "datasets/"
        file_name:
            str. The downloaded dataset will be saved using the "file_name". For example, "mnist_trainingset.mat"

    Return:
        str. The file path to the downloaded dataset. For example, "datasets/mnist_trainingset.mat"
    """
    # check file name
    if not file_name[-4:] == ".mat":
        raise NameError("file name should end up with \".mat\"!")

    # check saved path
    if not os.path.exists(saved_path):
        os.makedirs(saved_path)

    # avoid downloading the dataset again
    file_path = os.path.join(saved_path, file_name)
    if os.path.exists(file_path):
        return file_path

    # download dataset
    print("Downloading dataset from {}. Please wait...".format(url))
    file_stream = requests.get(url, stream=True)
    with open(file_path, 'wb') as local_file:
        for data in tqdm(file_stream):
            local_file.write(data)
    return file_path


def read_dataset(file_path: str) -> tuple:
    """
    Args:
        file_path:
            str. Path of the .mat file, for example, "datasets/mnist_trainingset.mat"

    Returns:
        tuple. The first element should have a shape of (N, H, W, C) and its pixel values should be in range (0, 255).
        The second element should have a shape of (N,)
    """
    # check file path
    if not file_path[-4:] == ".mat":
        raise NameError("file path should end up with \".mat\"!")

    data = scipy.io.loadmat(file_path)
    x, y = data["X"], np.squeeze(data["y"])
    return x, y


def save_dataset(x: np.ndarray, y: np.ndarray, saved_path: str) -> str:
    """
    Args:
        x:
            np.ndarray. Images to be saved. Images should have shape of (N, H, W, C)
        y:
            np.ndarray. Labels to be saved. Labels should have shape of (N,)
        saved_path:
            str. Path to save the .mat file. For example, "datasets/mnist_trainingset.mat"

    Returns:
        str. Path to save the .mat file. For example, "datasets/mnist_trainingset.mat"
    """
    # check type
    if not isinstance(x, np.ndarray):
        raise TypeError("x should be np.ndarray, but got {} instead!".format(x.__class__))
    elif not isinstance(y, np.ndarray):
        raise TypeError("y should be np.ndarray, but got {} instead!".format(y.__class__))

    # check dtype
    if not x.dtype == np.uint8:
        raise DataTypeError("x should have dtype np.uint8!")
    elif not y.dtype == np.uint8:
        raise DataTypeError("y should have dtype np.uint8!")

    # check shape
    if not x.ndim == 4:
        raise DimensionError("x should have 4 dimensions, but got {} instead!".format(x.ndim))
    elif not y.ndim == 1:
        raise DimensionError("y should have 1 dimensions, but got {} instead!".format(y.ndim))

    # check number of examples
    if not x.shape[0] == y.shape[0]:
        raise SampleNumberError("number of images and number of labels should be the same, but got {} images "
                                "and {} labels!".format(x.shape[0], y.shape[0]))

    # check save path
    if not saved_path[-4:] == ".mat":
        raise NameError("saved path should end up with \".mat\"! For example, \"./dataset.mat\"")

    # save dataset
    scipy.io.savemat(saved_path, dict(X=x, y=y))
    return saved_path


def visualize_images(images: np.ndarray, labels: np.ndarray, num_visualization: int = 9, num_per_row: int = 3) -> None:
    """
    Args:
        images:
            np.ndarray. Image to be visualized. Pixel value should be in range
            of (0, 1) or (0, 255)
        labels:
            np.ndarray. Corresponding labels
        num_visualization:
            int. Number of images to be visualized
        num_per_row:
            int. Number of images in one row of a big matplotlib figure
    """
    rows = math.ceil(num_visualization / num_per_row)
    for i in range(num_visualization):
        plt.subplot(rows, num_per_row, i + 1)
        plt.axis("off")
        plt.title("class #{}".format(labels[i]))
        plt.imshow(images[i])
    plt.show()


class DimensionError(Exception):
    pass


class SampleNumberError(Exception):
    pass


class DataTypeError(Exception):
    pass

